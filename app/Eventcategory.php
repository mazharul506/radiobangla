<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eventcategory extends Model
{
    protected $table='event_categories';
    protected $fillable=['name'];
    public function event(){
        return $this->hasMany('App\Event');
    }
}
