<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificationcategory extends Model
{
    protected $table='notificationcategories';
    protected $fillable=['name'];
}
