<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guestcategory extends Model
{
    protected $table='guestcategories';
    protected $fillable=['name'];
}
