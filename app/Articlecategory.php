<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articlecategory extends Model
{
    protected $table='articlecategories';
    protected $fillable=['name'];
}
