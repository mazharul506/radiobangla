<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programcategory extends Model
{
    protected $table='programcategories';
    protected $fillable=['name'];
}
