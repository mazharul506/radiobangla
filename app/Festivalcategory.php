<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Festivalcategory extends Model
{
    protected $table='festivalcategories';
    protected $fillable=['name'];
}
