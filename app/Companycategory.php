<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companycategory extends Model
{
    protected $table='companycategories';
    protected $fillable=['name'];
}
