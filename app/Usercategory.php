<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usercategory extends Model
{
    protected $table='usercategories';
    protected $fillable=['name'];
}
