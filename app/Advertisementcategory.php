<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisementcategory extends Model
{
    protected $table='advertismentcategories';
    protected $fillable=['name'];
}
