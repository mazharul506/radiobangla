<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groupcategory extends Model
{
    protected $table='groupcategories';
    protected $fillable=['name'];
}
