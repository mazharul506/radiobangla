<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table='events';
    protected $fillable = [
        'eventcategories_id','title','short_description','htmlized_description','date','start','end','type','status','venue','contact','picture','content'
    ];
    public function eventcategory(){
        return $this->belongsTo('App\Eventcategory');
    }

}
