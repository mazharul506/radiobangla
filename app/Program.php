<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $fillable = [
        'title','short_description','htmlized_description','type','status','picture','content'
    ];
}
