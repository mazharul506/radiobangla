<?php

namespace App\Http\Controllers;

use App\Contestcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ContestcategoryController extends Controller
{
    public function index()
    {
        $categories = Contestcategory::paginate(25);

        return view('admin.contestcategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.contestcategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Contestcategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/contestcategories');
    }


    public function show($id)
    {
        $category = Contestcategory::findOrFail($id);

        return view('admin.contestcategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Contestcategory::findOrFail($id);

        return view('admin.contestcategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Contestcategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/contestcategories');
    }

    public function destroy($id)
    {
        Contestcategory::destroy($id);

        Session::flash('flash_message', 'category deleted!');

        return redirect('/contestcategories');
    }
}
