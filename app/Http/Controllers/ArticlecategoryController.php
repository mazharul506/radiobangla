<?php

namespace App\Http\Controllers;


use App\Articlecategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ArticlecategoryController extends Controller
{
    public function index()
    {
        $categories = Articlecategory::paginate(25);

        return view('admin.articlecategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.articlecategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Articlecategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/articlecategories');
    }


    public function show($id)
    {
        $category = Articlecategory::findOrFail($id);

        return view('admin.articlecategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Articlecategory::findOrFail($id);

        return view('admin.articlecategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Articlecategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/articlecategories');
    }

    public function destroy($id)
    {
        Articlecategory::destroy($id);

        Session::flash('flash_message', 'category deleted!');

        return redirect('/articlecategories');
    }
}
