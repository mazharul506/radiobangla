<?php

namespace App\Http\Controllers;

use App\Program;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use \Input as Input;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\Node\Scalar\MagicConst\Dir;

class ProgramController extends Controller
{
    public function index()
    {
        $allPrograms = DB::table('programs')->orderBy('id','desc')->get();
        return view("admin.programs.index",['programs'=>$allPrograms]);
    }
    public function create()
    {
        return view("admin.programs.create");
    }

    public function store(Request $request)
    {
        $this->validate($request,['title'=>'required|min:5|max:50']);
        $picture_file = $request->file('picture');
        if(isset($picture_file) && !empty($picture_file)){
            $picture_name = $picture_file->getClientOriginalName();
            $picture_destination_path = "uploads/programs/";
            $picture_file->move($picture_destination_path,$picture_name);
        }else{
            $picture_name = '';
        }
        $data = $request->only('title','short_description','description','htmlized_description','type');
        $data['status']=0;
        $data['picture']=$picture_name;

        if (isset($request->save)){
            Program::create($data);
            Session::flash('msg','Data Inserted Successfully');
            return redirect('/programs');
        }
        elseif (isset($request->save_and_edit)){
            $program = Program::create($data);
            $program = Program::findorfail($program->id);
            return view('admin.programs.edit',['program'=>$program]);
        }
    }
    public function show($id)
    {
        $program = Program::find($id);
        return view('admin.programs.view',['program'=>$program]);
    }
    public function pdf($id,$title)
    {
        $program = Program::find($id);
        $pdf = PDF::loadView('admin.programs.pdf',['program'=>$program])->setPaper('a4','portrait');
        return $pdf->download($title.".pdf");
    }

    public function listPdf()
    {
        $allPrograms = DB::table('programs')->orderBy('id','desc')->get();
        $pdf = PDF::loadView('admin.programs.listPdf',['programs'=>$allPrograms])->setPaper('a4','portrait');
        return $pdf->download('program-list.pdf');
    }

    public function edit($id)
    {
        $program = Program::findorfail($id);
        return view('admin.programs.edit',['program'=>$program]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,['title'=>'required|min:5|max:50']);
        $program = Program::findorfail($id);
        $picture_file = $request->file('picture');
        if (isset( $picture_file)){
            $picture_name = $picture_file->getClientOriginalName();
            $picture_destination_path = "uploads/programs/";
            $picture_file->move($picture_destination_path,$picture_name);
        }else{
            $picture_name = $program->picture;
        }
        $data = $request->only('title','short_description','htmlized_description','picture','type');
        $data['status']=0;
        $data['picture'] = $picture_name;
        $program->update($data);
        Session::flash('msg','Data Updated Successfully');
        return redirect('/programs');
    }
    public function destroy($id,$title)
    {
        $program = Program::findorfail($id);
        if ($program->title == $title){
            $program->delete();
            Session::flash('msg','Data Deleted Successfully');
        }else{
            Session::flash('msg','Failed To Delete...');
        }
        return redirect('/programs');
    }


    public function downloadExcel($type){
        $program = Program::all('title','short_description','htmlized_description','picture','type')->toArray();
        return Excel::create('program.',function($excel) use ($program){
            $excel->sheet('mySheet',function($sheet) use ($program){
                $sheet->fromArray($program);
            });
        })->download($type);

    }

    public function printPreview($id)
    {
        $program = Program::find($id);
//        return view ('admin.programs.printview',compact('programs'));
        return view('admin.programs.printview', ['program' => $program]);
    }
    public function printAll(){
        $program = Program::all();
//        return view ('admin.programs.printview',compact('programs'));
        return view('admin.programs.printview',['program'=>$program]);
    }

    public function printsingleview($id){
        $program = Program::find($id);
//        return view ('admin.programs.printview',compact('programs'));
        return view('admin.programs.printsingleview',['program'=>$program]);
    }

}
