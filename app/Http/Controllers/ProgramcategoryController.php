<?php

namespace App\Http\Controllers;

use App\Programcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class ProgramcategoryController extends Controller
{
    public function index()
    {
        $categories = Programcategory::paginate(25);

        return view('admin.programcategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.programcategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Programcategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/programcategories');
    }


    public function show($id)
    {
        $category = Programcategory::findOrFail($id);

        return view('admin.programcategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Programcategory::findOrFail($id);

        return view('admin.programcategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Programcategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/programcategories');
    }

    public function destroy($id)
    {
        Programcategory::destroy($id);

        Session::flash('flash_message', 'Post deleted!');

        return redirect('/programcategories');
    }
}
