<?php

namespace App\Http\Controllers;

use App\Archivecategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ArchivecategoryController extends Controller
{
    public function index()
    {
        $categories = Archivecategory::paginate(25);

        return view('admin.archivecategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.archivecategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Archivecategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/archivecategories');
    }


    public function show($id)
    {
        $category = Archivecategory::findOrFail($id);

        return view('admin.archivecategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Archivecategory::findOrFail($id);

        return view('admin.archivecategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Archivecategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/archivecategories');
    }

    public function destroy($id)
    {
        Archivecategory::destroy($id);

        Session::flash('flash_message', 'category deleted!');

        return redirect('/archivecategories');
    }
}
