<?php

namespace App\Http\Controllers;

use App\Eventcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EventcategoryController extends Controller
{
    public function index()
    {
        $eventcategory=Eventcategory::all();
        return view('admin.eventcategories.index',['eventcategories'=>$eventcategory]);
    }


    public function create()
    {
        return view('admin.eventcategories.create');
    }


    public function store(Request $request)
    {
        $data = $request->only('name');
        if (isset($request->save)){
            Eventcategory::create($data);
            Session::flash('msg','Data Inserted Successfully');
            return redirect('/eventcategories');
        }
        elseif (isset($request->save_and_edit)){
            $eventcategory = Eventcategory::create($data);
            $eventcategory=Eventcategory::findorfail($eventcategory->id);
            return view('admin.eventcategories.edit',['eventcategory'=>$eventcategory]);
        }

    }


    public function show($id)
    {
        $eventcategory = Eventcategory::find($id);
        return view('admin.eventcategories.view',['eventcategory'=>$eventcategory]);
    }


    public function edit($id)
    {
        $eventcategory=Eventcategory::find($id);

        return view('admin.eventcategories.edit',['eventcategory'=> $eventcategory]);
    }


   public function update(Request $request, $id)
    {
        $data = $request->only('name');
        $eventcategory = Eventcategory::findorfail($id);
        $eventcategory ->update($data);
        Session::flash('msg','Data Updated Successfully');
        return redirect('/eventcategories');

    }

   public function destroy($id ,$name)
   {
       $eventcategory = Eventcategory::findorfail($id);
       if ($eventcategory->name == $name){
           $eventcategory->delete();
           Session::flash('msg','Data Deleted Successfully');
            }else
                {
                    Session::flash('msg','Failed To Delete...');
            }
               return redirect('/eventcategories');
               }
}
