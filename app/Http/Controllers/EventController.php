<?php

namespace App\Http\Controllers;

use App\Event;
use App\Eventcategory;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;


class EventController extends Controller
{
    public function index()
    {
//       $allEvents = DB::table('events')->orderBy('id','desc')->get();
//        $allEvents=Event::paginate(25);
        $allEvents=Event::with('eventcategory')->get();
//        dd($allEvents);
        return view("admin.events.index",['events'=>$allEvents]);
    }
    public function create()
    {
        $eventcategory=Eventcategory::pluck('name','id');
        return view ('admin.events.create',['eventcategories'=>$eventcategory]);

    }

    public function store(Request $request)
    {
        $this->validate($request,['title'=>'required|min:5|max:50']);
        $picture_file = $request->file('picture');
        if(isset($picture_file) && !empty($picture_file)){
            $picture_name = $picture_file->getClientOriginalName();
            $picture_destination_path = "uploads/events/";
            $picture_file->move($picture_destination_path,$picture_name);
        }else{
            $picture_name = '';
        }
        $data = $request->only('eventcategories_id','title','short_description','htmlized_description','date','start','end','type','status','venue','contact','picture','content');
        $data['status']=0;
        $data['picture']=$picture_name;

        if (isset($request->save)){
            Event::create($data);
            Session::flash('msg','Data Inserted Successfully');
            return redirect('/events');
        }
        elseif (isset($request->save_and_edit)){
            $event = Event::create($data);
            $event= Event::findorfail($event->id);
            $eventcategory=Eventcategory::pluck('name','id');
            return view('admin.events.edit',['event'=>$event,'eventcategories'=>$eventcategory]);
        }
    }
    public function show($id)
    {

        $data=Event::with('eventcategory')->find($id);
        //        return view('admin.events.view',['event'=>$event]);


        return view ('admin.events.view',['event'=>$data]);

    }
    public function pdf($id,$title)
    {
        $event = Event::find($id);
        $pdf = PDF::loadView('admin.events.pdf',['event'=>$event])->setPaper('a4','portrait');
        return $pdf->download($title.".pdf");
    }

    public function listPdf()
    {
        $allEvents = DB::table('events')->orderBy('id','desc')->get();
        $pdf = PDF::loadView('admin.events.listPdf',['events'=>$allEvents])->setPaper('a4','portrait');
        return $pdf->download('event-list.pdf');
    }

    public function edit($id)
    {
//        $event = Event::findorfail($id);
//        return view('admin.events.edit',['event'=>$event]);
        $event=Event::findorfail($id);
        $eventcategory=Eventcategory::pluck('name','id');
        return view ('admin.events.edit',['event'=>$event,'eventcategories'=>$eventcategory]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,['title'=>'required|min:5|max:50']);
        $event = Event::findorfail($id);
        $picture_file = $request->file('picture');
        if (isset( $picture_file)){
            $picture_name = $picture_file->getClientOriginalName();
            $picture_destination_path = "uploads/events/";
            $picture_file->move($picture_destination_path,$picture_name);
        }else{
            $picture_name = $event->picture;
        }
        $data = $request->only('eventcategories_id','title','short_description','htmlized_description','date','start','end','type','status','venue','contact','picture','content');
        $data['status']=0;
        $data['picture'] = $picture_name;
        $event->update($data);
        Session::flash('msg','Data Updated Successfully');
        return redirect('/events');
    }
    public function destroy($id,$title)
    {
        $event = Event::findorfail($id);
        if ($event->title == $title){
            $event->delete();
            Session::flash('msg','Data Deleted Successfully');
        }else{
            Session::flash('msg','Failed To Delete...');
        }
        return redirect('/events');
    }


    public function downloadExcel($type){
        $event = Event::all('title','short_description','htmlized_description','date','start','end','type','status','venue','contact','picture')->toArray();
        return Excel::create('event.',function($excel) use ($event){
            $excel->sheet('mySheet',function($sheet) use ($event){
                $sheet->fromArray($event);
            });
        })->download($type);

    }

    public function printPreview($id)
    {
        $event = Event::find($id);
//        return view ('admin.programs.printview',compact('programs'));
        return view('admin.events.printview', ['event' => $event]);
    }
    public function printAll(){
        $event = Event::all();
//        return view ('admin.programs.printview',compact('programs'));
        return view('admin.events.printview',['event'=>$event]);
    }

    public function printsingleview($id){
        $event = Event::find($id);
//        return view ('admin.programs.printview',compact('programs'));
        return view('admin.events.printsingleview',['event'=>$event]);
    }

}
