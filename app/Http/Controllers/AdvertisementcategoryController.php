<?php

namespace App\Http\Controllers;

use App\Advertisementcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdvertisementcategoryController extends Controller
{
    public function index()
    {
        $categories = Advertisementcategory::paginate(25);

        return view('admin.advertisementcategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.advertisementcategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Advertisementcategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/guestcategories');
    }


    public function show($id)
    {
        $category = Advertisementcategory::findOrFail($id);

        return view('admin.advertisementcategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Advertisementcategory::findOrFail($id);

        return view('admin.advertisementcategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Advertisementcategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/advertisementcategories');
    }

    public function destroy($id)
    {
        Advertisementcategory::destroy($id);

        Session::flash('flash_message', 'category deleted!');

        return redirect('/advertisementcategories');
    }
}
