<?php

namespace App\Http\Controllers;

use App\Schedulecategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SchedulecategoryController extends Controller
{
    public function index()
    {
        $categories = Schedulecategory::paginate(25);

        return view('admin.schedulecategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.schedulecategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Schedulecategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/schedulecategories');
    }


    public function show($id)
    {
        $category = Schedulecategory::findOrFail($id);

        return view('admin.schedulecategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Schedulecategory::findOrFail($id);

        return view('admin.schedulecategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Schedulecategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/schedulecategories');
    }

    public function destroy($id)
    {
        Schedulecategory::destroy($id);

        Session::flash('flash_message', 'category deleted!');

        return redirect('/schedulecategories');
    }
}
