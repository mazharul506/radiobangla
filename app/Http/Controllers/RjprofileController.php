<?php

namespace App\Http\Controllers;

use App\Rjprofile;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class RjprofileController extends Controller
{
    public function index()
    {
        $allRjs = DB::table('rjprofiles')->orderBy('id','desc')->get();
        return view("admin.rj-profiles.index",['rjs'=>$allRjs]);
    }
    public function create()
    {
        return view("admin.rj-profiles.create");
    }
    public function store(Request $request)
    {

        $this->validate($request,['name'=>'required|min:5|max:50']);
        $this->validate($request,['picture'=>'mimes:png,jpg,jpeg|max:2048']);
        $this->validate($request,['contact'=>'numeric']);
        $this->validate($request,['email'=>'email|max:255|unique:rjprofiles']);


        $picture_file = $request->file('picture');
        if(isset($picture_file) && !empty($picture_file)){
            $picture_name = time().'.'.$picture_file->getClientOriginalName();
            $picture_destination_path = "uploads/rj-profiles/";
            $picture_file->move($picture_destination_path,$picture_name);
        }else{
            $picture_name = '';
        }


        $data = $request->only('name','email','contact','date_of_birth','favourite_music','tv_shows','favourite_places_to_shop','happy_place','for_fun');
        $data['picture']=$picture_name;
        $data['password']=date('His');
        //dd($data);
        if (isset($request->save)){
            Rjprofile::create($data);
            Session::flash('msg','Data Inserted Successfully');
            return redirect('/rj-profiles');
        }
        elseif (isset($request->save_and_edit)){
            $rj = Rjprofile::create($data);
            $rj = Rjprofile::findorfail($rj->id);
            return view('admin.rj-profiles.edit',['rj'=>$rj]);
        }
    }
    public function show($id)
    {
        $rj = Rjprofile::find($id);
        return view('admin.rj-profiles.view',['rj'=>$rj]);
    }
    public function edit($id)
    {
        $rj = Rjprofile::findorfail($id);
        return view('admin.rj-profiles.edit',['rj'=>$rj]);
    }
    public function pdf($id,$title)
    {
        $rjProfile = Rjprofile::find($id);
        $pdf = PDF::loadView('admin.rj-profiles.pdf',['rj'=>$rjProfile])->setPaper('a4','portrait');
        return $pdf->download($title.".pdf");
    }
    public function listPdf()
    {
        $allRjs = DB::table('rjprofiles')->orderBy('id','desc')->get();
        $pdf = PDF::loadView('admin.rj-profiles.listPdf',['rjs'=>$allRjs])->setPaper('a4','portrait');
        return $pdf->download('rs-list.pdf');
    }
    public function printAll(){
        $rjs = Rjprofile::all();
        return view('admin.rj-profiles.printAll',['rjs'=>$rjs]);
    }
    public function printSingle($id){
        $rj = Rjprofile::find($id);
        return view('admin.rj-profiles.printSingle',['rj'=>$rj]);
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,['name'=>'required|min:5|max:50']);
        $this->validate($request,['picture'=>'mimes:png,jpg,jpeg|max:2048']);
        $this->validate($request,['contact'=>'numeric']);

        $rj = Rjprofile::findorfail($id);
        $picture_file = $request->file('picture');
        if (isset( $picture_file)){
            $picture_name = time().'.'.$picture_file->getClientOriginalName();
            $picture_destination_path = "uploads/rj-profiles/";
            $picture_file->move($picture_destination_path,$picture_name);
        }else{
            $picture_name = $rj->picture;
        }
        $data = $request->only('name','email','contact','date_of_birth','favourite_music','tv_shows','favourite_places_to_shop','happy_place','for_fun');
        $email = $data['email'];
        if (isset($email) && !empty($email)){
            if ($rj->email == $email){
                $data['email'] = $email;
            }else{
                $this->validate($request,['email'=>'email|max:255|unique:rjprofiles']);
            }
        }

        $data['status']=0;
        $data['picture'] = $picture_name;

        $rj->update($data);
        Session::flash('msg','Data Updated Successfully');
        return redirect('/rj-profiles');
    }
    public function downloadExcel(){
        $rjProfiles = Rjprofile::all('id','name','email','date_of_birth')->toArray();
        return Excel::create('Rjprofiles.',function($excel) use ($rjProfiles){
            $excel->sheet('mySheet',function($sheet) use ($rjProfiles){
                $sheet->fromArray($rjProfiles);
            });
        })->download("xlsx");
    }
    public function destroy($id,$name)
    {
        $rj = Rjprofile::findorfail($id);
        if ($rj->name == $name){
            $rj->delete();
            Session::flash('msg','Data Deleted Successfully');
        }else{
            Session::flash('msg','Failed To Delete...');
        }
        return redirect('/rj-profiles');
    }
}
