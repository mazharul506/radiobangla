<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\UserProfile;
use Illuminate\Http\Request;
use Session;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $userprofile = UserProfile::paginate(25);

        return view('admin.user-profile.index', compact('userprofile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.user-profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        

if ($request->hasFile('picture')) {
    $uploadPath = public_path('/uploads/');

    $extension = $request->file('picture')->getClientOriginalExtension();
    $fileName = rand(11111, 99999) . '.' . $extension;

    $request->file('picture')->move($uploadPath, $fileName);
    $requestData['picture'] = $fileName;
}

        UserProfile::create($requestData);

        Session::flash('flash_message', 'UserProfile added!');

        return redirect('admin/user-profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $userprofile = UserProfile::findOrFail($id);

        return view('admin.user-profile.show', compact('userprofile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $userprofile = UserProfile::findOrFail($id);

        return view('admin.user-profile.edit', compact('userprofile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        

if ($request->hasFile('picture')) {
    $uploadPath = public_path('/uploads/');

    $extension = $request->file('picture')->getClientOriginalExtension();
    $fileName = rand(11111, 99999) . '.' . $extension;

    $request->file('picture')->move($uploadPath, $fileName);
    $requestData['picture'] = $fileName;
}

        $userprofile = UserProfile::findOrFail($id);
        $userprofile->update($requestData);

        Session::flash('flash_message', 'UserProfile updated!');

        return redirect('admin/user-profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        UserProfile::destroy($id);

        Session::flash('flash_message', 'UserProfile deleted!');

        return redirect('admin/user-profile');
    }
}
