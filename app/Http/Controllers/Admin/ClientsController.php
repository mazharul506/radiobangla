<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Client;
use Illuminate\Http\Request;
use Session;
use Maatwebsite\Excel\Facades\Excel;


class ClientsController extends Controller
{

    public function index()
    {
        $clients = Client::paginate(25);
        return view('admin.clients.index', compact('clients'));
    }

    public function create()
    {
        return view('admin.clients.create');
    }

    public function store(Request $request)
    {
        $requestData = $request->all();
        if ($request->hasFile('attachment')) {
            $uploadPath = public_path('/uploads/clients-attachment/');

            $extension = $request->file('attachment')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('attachment')->move($uploadPath, $fileName);
            $requestData['attachment'] = $fileName;
        }

        Client::create($requestData);

        Session::flash('flash_message', 'Client added!');

        return redirect('admin/clients');
    }

    public function show($id)
    {
        $client = Client::findOrFail($id);

        return view('admin.clients.show', compact('client'));
    }

    public function edit($id)
    {
        $client = Client::findOrFail($id);

        return view('admin.clients.edit', compact('client'));
    }

    public function update($id, Request $request)
    {
        $requestData = $request->all();
        if ($request->hasFile('attachment')) {
            $uploadPath = public_path('/uploads/attachment/');

            $extension = $request->file('attachment')->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $request->file('attachment')->move($uploadPath, $fileName);
            $requestData['attachment'] = $fileName;
        }

        $client = Client::findOrFail($id);
        $client->update($requestData);

        Session::flash('flash_message', 'Client updated!');

        return redirect('admin/clients');
    }
    public function downloadExcel(){
        $clients = Client::all('title','types_of_company','address','state','country','zip_code','email','phone_1','phone_2','website','contact_name','contact_designation','contact_phone_1','contact_phone_2',
            'contact_email','assigned_person')->toArray();
        return Excel::create('clients.',function($excel) use ($clients){
            $excel->sheet('mySheet',function($sheet) use ($clients){
                $sheet->fromArray($clients);
            });
        })->download("xlsx");
    }
    public function importExcel(Request $request)
    {
        if($request->hasFile('import_file')){
            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get();
            if(!empty($data) && $data->count()){
                foreach ($data->toArray() as $key => $value) {
                    if(!empty($value)){
                        if (isset($value['title'])&& isset($value['types_of_company'])) {
                            $insert[] = ['title' => $value['title'],'types_of_company' => $value['types_of_company'], 'address' => $value['address'], 'state' => $value['state'],
                                'country' => $value['country'], 'zip_code' => $value['zip_code'], 'email' => $value['email'],
                                'phone_1' => $value['phone_1'], 'phone_2' => $value['phone_2'], 'website' => $value['website'],
                                'contact_name' => $value['contact_name'], 'contact_designation' => $value['contact_designation'], 'contact_phone_1' => $value['contact_phone_1'],
                                'contact_phone_2' => $value['contact_phone_2'], 'contact_email' => $value['contact_email'], 'assigned_person' => $value['assigned_person']
                            ];
                        }
                    }
                }
                if(!empty($insert)){
                    Client::insert($insert);
                    return back()->with('success','Insert Record successfully.');
                }
            }
        }
        return back()->with('error','Please Check your file, Something is wrong there.');
    }
    public function destroy($id)
    {
        Client::destroy($id);

        Session::flash('flash_message', 'Client deleted!');

        return redirect('admin/clients');
    }
}
