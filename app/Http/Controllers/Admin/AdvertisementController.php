<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Advertisement;
use Illuminate\Http\Request;
use Session;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $advertisement = Advertisement::paginate(25);

        return view('admin.advertisement.index', compact('advertisement'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.advertisement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        

if ($request->hasFile('image')) {
    $uploadPath = public_path('/uploads/');

    $extension = $request->file('image')->getClientOriginalExtension();
    $fileName = rand(11111, 99999) . '.' . $extension;

    $request->file('image')->move($uploadPath, $fileName);
    $requestData['image'] = $fileName;
}

        Advertisement::create($requestData);

        Session::flash('flash_message', 'Advertisement added!');

        return redirect('admin/advertisement');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $advertisement = Advertisement::findOrFail($id);

        return view('admin.advertisement.show', compact('advertisement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $advertisement = Advertisement::findOrFail($id);

        return view('admin.advertisement.edit', compact('advertisement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        

if ($request->hasFile('image')) {
    $uploadPath = public_path('/uploads/');

    $extension = $request->file('image')->getClientOriginalExtension();
    $fileName = rand(11111, 99999) . '.' . $extension;

    $request->file('image')->move($uploadPath, $fileName);
    $requestData['image'] = $fileName;
}

        $advertisement = Advertisement::findOrFail($id);
        $advertisement->update($requestData);

        Session::flash('flash_message', 'Advertisement updated!');

        return redirect('admin/advertisement');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Advertisement::destroy($id);

        Session::flash('flash_message', 'Advertisement deleted!');

        return redirect('admin/advertisement');
    }
}
