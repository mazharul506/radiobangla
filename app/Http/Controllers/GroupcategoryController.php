<?php

namespace App\Http\Controllers;

use App\Groupcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GroupcategoryController extends Controller
{
    public function index()
    {
        $categories = Groupcategory::paginate(25);

        return view('admin.groupcategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.groupcategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Groupcategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/groupcategories');
    }


    public function show($id)
    {
        $category = Groupcategory::findOrFail($id);

        return view('admin.groupcategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Groupcategory::findOrFail($id);

        return view('admin.groupcategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Groupcategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/groupcategories');
    }

    public function destroy($id)
    {
        Groupcategory::destroy($id);

        Session::flash('flash_message', 'category deleted!');

        return redirect('/groupcategories');
    }
}
