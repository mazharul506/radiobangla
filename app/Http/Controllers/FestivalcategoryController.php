<?php

namespace App\Http\Controllers;

use App\Festivalcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FestivalcategoryController extends Controller
{
    public function index()
    {
        $categories = Festivalcategory::paginate(25);

        return view('admin.festivalcategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.festivalcategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Festivalcategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/festivalcategories');
    }


    public function show($id)
    {
        $category = Festivalcategory::findOrFail($id);

        return view('admin.festivalcategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Festivalcategory::findOrFail($id);

        return view('admin.festivalcategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Festivalcategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/festivalcategories');
    }

    public function destroy($id)
    {
        Festivalcategory::destroy($id);

        Session::flash('flash_message', 'category deleted!');

        return redirect('/festivalcategories');
    }
}
