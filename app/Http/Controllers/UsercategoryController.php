<?php

namespace App\Http\Controllers;

use App\Usercategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UsercategoryController extends Controller
{
    public function index()
    {
        $categories = Usercategory::paginate(25);

        return view('admin.usercategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.usercategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Usercategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/usercategories');
    }


    public function show($id)
    {
        $category = Usercategory::findOrFail($id);

        return view('admin.usercategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Usercategory::findOrFail($id);

        return view('admin.usercategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Usercategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/usercategories');
    }

    public function destroy($id)
    {
        Usercategory::destroy($id);

        Session::flash('flash_message', 'category deleted!');

        return redirect('/usercategories');
    }
}
