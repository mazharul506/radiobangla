<?php

namespace App\Http\Controllers;

use App\Notificationcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NotificationcategoryController extends Controller
{
    public function index()
    {
        $categories = Notificationcategory::paginate(25);

        return view('admin.notificationcategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.notificationcategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Notificationcategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/notificationcategories');
    }


    public function show($id)
    {
        $category = Notificationcategory::findOrFail($id);

        return view('admin.notificationcategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Notificationcategory::findOrFail($id);

        return view('admin.notificationcategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Notificationcategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/notificationcategories');
    }

    public function destroy($id)
    {
        Notificationcategory::destroy($id);

        Session::flash('flash_message', 'category deleted!');

        return redirect('/notificationcategories');
    }
}
