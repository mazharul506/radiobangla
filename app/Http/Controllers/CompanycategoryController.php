<?php

namespace App\Http\Controllers;

use App\Companycategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CompanycategoryController extends Controller
{
    public function index()
    {
        $categories = Companycategory::paginate(25);

        return view('admin.companycategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.companycategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Companycategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/companycategories');
    }


    public function show($id)
    {
        $category = Companycategory::findOrFail($id);

        return view('admin.companycategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Companycategory::findOrFail($id);

        return view('admin.companycategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Companycategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/companycategories');
    }

    public function destroy($id)
    {
        Companycategory::destroy($id);

        Session::flash('flash_message', 'category deleted!');

        return redirect('/companycategories');
    }
}
