<?php

namespace App\Http\Controllers;

use App\Rjprofile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class RjController extends Controller
{
    public function index($id,$pass)
    {
        $rj = Rjprofile::find($id);
        if ($rj->password == $pass){
            return view('admin.rjs.index',['rj'=>$rj]);
        }else{
            return redirect('/rj-login');
        }
    }
    public function login()
    {
        return view('admin.rjs.login');
    }
    public function loginProcess(Request $request)
    {
        $this->validate($request,['email'=>'required|min:5|max:50']);
        $this->validate($request,['password'=>'required|min:5']);
        $rj = DB::table('rjprofiles')->where('email',$request->only('email'))
                ->where('password',$request->only('password'))
                ->first();
        if (isset($rj->name) && !empty($rj->name) && isset($rj->password) && !empty($rj->password)){
            return redirect("/my-profile/$rj->id/$rj->password");
        }else{
            Session::flash('msg','Incorrect Credentials');
            return redirect('/rj-login');
        }
    }
    public function show($id,$pass)
    {
        $rj = Rjprofile::find($id);
        if ($rj->password == $pass){
            return view('admin.rjs.view',['rj'=>$rj]);
        }else{
            return redirect('/rj-login');
        }
    }
    public function edit($id,$pass)
    {
        $rj = Rjprofile::findorfail($id);
        if ($rj->password == $pass){
            return view('admin.rjs.edit',['rj'=>$rj]);
        }else{
            return redirect('/rj-login');
        }
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,['picture'=>'mimes:png,jpg,jpeg|max:2048']);
        $this->validate($request,['contact'=>'numeric']);
        $rj = Rjprofile::findorfail($id);
        $picture_file = $request->file('picture');
        if (isset( $picture_file)){
            $picture_name = time().'.'.$picture_file->getClientOriginalName();
            $picture_destination_path = "uploads/rj-profiles/";
            $picture_file->move($picture_destination_path,$picture_name);
        }else{
            $picture_name = $rj->picture;
        }
        $data = $request->only('email','contact','date_of_birth','favourite_music','tv_shows','favourite_places_to_shop','happy_place','for_fun');
        $email = $data['email'];
        if (isset($email) && !empty($email)){
            if ($rj->email == $email){
                $data['email'] = $email;
            }else{
                $this->validate($request,['email'=>'email|max:255|unique:rjprofiles']);
            }
        }
        $data['status']=0;
        $data['picture'] = $picture_name;
        $rj->update($data);
        Session::flash('msg','Data Updated Successfully');
        return redirect("/".rtrim($request->path(),"/update"));
    }
}
