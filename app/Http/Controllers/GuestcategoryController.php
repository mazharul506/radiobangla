<?php

namespace App\Http\Controllers;

use App\Guestcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GuestcategoryController extends Controller
{
    public function index()
    {
        $categories = Guestcategory::paginate(25);

        return view('admin.guestcategories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.guestcategories.create');
    }

    public function store(Request $request)
    {

        $requestData = $request->all();

        Guestcategory::create($requestData);

        Session::flash('flash_message', 'Category added!');

        return redirect('/guestcategories');
    }


    public function show($id)
    {
        $category = Guestcategory::findOrFail($id);

        return view('admin.guestcategories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Guestcategory::findOrFail($id);

        return view('admin.guestcategories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $category = Guestcategory::findOrFail($id);
        $category->update($requestData);

        Session::flash('flash_message', 'Category updated!');

        return redirect('/guestcategories');
    }

    public function destroy($id)
    {
        Guestcategory::destroy($id);

        Session::flash('flash_message', 'category deleted!');

        return redirect('/guestcategories');
    }
}
