<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rjprofile extends Model
{
    protected $fillable = [
        'name','picture','email','contact','date_of_birth','favourite_music','tv_shows','favourite_places_to_shop','happy_place','for_fun','password',
    ];
}
