<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contestcategory extends Model
{
    protected $table='contestcategories';
    protected $fillable=['name'];
}
