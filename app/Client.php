<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clients';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title','types_of_company', 'address', 'state', 'country', 'zip_code', 'email', 'phone_1', 'phone_2', 'website', 'contact_name', 'contact_designation', 'contact_phone_1', 'contact_phone_2', 'contact_email', 'assigned_person', 'attachment'];

    
}
