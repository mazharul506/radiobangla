<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivecategory extends Model
{
    protected $table='archivecategories';
    protected $fillable=['name'];
}
