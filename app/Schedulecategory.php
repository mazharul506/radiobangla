<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedulecategory extends Model
{
    protected $table='schedulecategories';
    protected $fillable=['name'];
}
