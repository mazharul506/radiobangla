<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Program;

Route::get('/', function () {
    return view('welcome');
});


//////////////program
Route::get('/programs/program-list-pdf-download',"ProgramController@listPdf");

Route::get('/programs',"ProgramController@index");
Route::get('/programs/create',"ProgramController@create");
Route::post('/programs/store',"ProgramController@store");
Route::get('/programs/{id}',"ProgramController@show");
Route::get('/programs/{id}/edit',"ProgramController@edit");
Route::post('/programs/{id}/update',"ProgramController@update");
Route::get('/programs/{id}/{title}/delete',"ProgramController@destroy");
Route::get('/programs/{id}/{title}/pdf',"ProgramController@pdf");
Route::get('downloadExcel/{type}','ProgramController@downloadExcel');
Route::get('printAll','ProgramController@printAll');
Route::get('printsingleview/{id}','ProgramController@printsingleview');

///////////// rj-profiles
Route::get('/rj-profiles/rj-list-excel-download','RjprofileController@downloadExcel');
Route::get('/rj-profiles/rj-list-pdf-download',"RjprofileController@listPdf");
Route::get('/rj-profiles/rj-list-print','RjprofileController@printAll');
Route::get('/rj-profiles/create',"RjprofileController@create");
Route::post('/rj-profiles/store',"RjprofileController@store");
Route::get('/rj-profiles',"RjprofileController@index");
Route::get('/rj-profiles/{id}',"RjprofileController@show");
Route::get('/rj-profiles/{id}/edit',"RjprofileController@edit");
Route::post('/rj-profiles/{id}/update',"RjprofileController@update");
Route::get('/rj-profiles/{id}/{name}/delete',"RjprofileController@destroy");
Route::get('/rj-profiles/downloadExcel','RjprofileController@downloadExcel');
Route::get('/rj-profiles/{id}/{title}/pdf',"RjprofileController@pdf");
Route::get('/rj-profiles/print/{id}','RjprofileController@printSingle');

//////////////// rj
Route::post('/rj-login-process',"RjController@loginProcess");
Route::get('/rj-login',"RjController@login");
Route::get('/my-profile/{id}/{pass}',"RjController@index");
Route::get('/my-profile/{id}/{pass}/view',"RjController@show");
Route::get('/my-profile/{id}/{pass}/edit',"RjController@edit");
Route::post('/my-profile/{id}/{pass}/update',"RjController@update");

////////////////schedule
Route::get('/schedule',"ScheduleController@index");

///////////////clients
Route::get('/admin/clients/downloadExcel','Admin\\ClientsController@downloadExcel');
Route::post('/admin/clients/importExcel','Admin\\ClientsController@importExcel');
Route::resource('admin/clients', 'Admin\\ClientsController');

//////////////advertisement
Route::resource('admin/advertisement', 'Admin\\AdvertisementController');

/////////////contest
Route::resource('admin/contest', 'Admin\\ContestController');

/////////////group
Route::resource('admin/group', 'Admin\\GroupController');
Route::resource('admin/user-profile', 'Admin\\UserProfileController');

//////////////event

Route::get('/events/event-list-pdf-download',"EventController@listPdf");
Route::get('/events',"EventController@index");
Route::get('/events/create',"EventController@create");
Route::post('/events/store',"EventController@store");
Route::get('/events/{id}',"EventController@show");
Route::get('/events/{id}/edit',"EventController@edit");
Route::post('/events/{id}/update',"EventController@update");
Route::get('/events/{id}/{title}/delete',"EventController@destroy");
Route::get('/events/{id}/{title}/pdf',"EventController@pdf");
Route::get('downloadExcel/{type}','EventController@downloadExcel');
Route::get('printAll','EventController@printAll');
Route::get('printsingleview/{id}','EventController@printsingleview');

//event category
Route::get('/eventcategories',"EventcategoryController@index");
Route::get('/eventcategories/create',"EventcategoryController@create");
Route::post('/eventcategories/store',"EventcategoryController@store");
Route::get('/eventcategories/{id}',"EventcategoryController@show");
Route::get('/eventcategories/{id}/edit',"EventcategoryController@edit");
Route::post('/eventcategories/{id}/update',"EventcategoryController@update");
Route::get('/eventcategories/{id}/{name}/delete',"EventcategoryController@destroy");

//program category
Route::resource('/programcategories', 'ProgramcategoryController');

//group category
Route::resource('/groupcategories', 'GroupcategoryController');

//guest category
Route::resource('/guestcategories', 'GuestcategoryController');

//schedule categories
Route::resource('/schedulecategories', 'SchedulecategoryController');

//advertisement category
Route::resource('/advertisementcategories', 'AdvertisementcategoryController');
//Archive category
Route::resource('/archivecategories', 'ArchivecategoryController');
//guest category
Route::resource('/companycategories', 'CompanycategoryController');

//article category
Route::resource('/articlecategories', 'ArticlecategoryController');
//Festival category
Route::resource('/festivalcategories', 'FestivalcategoryController');

//contest category
Route::resource('/contestcategories', 'ContestcategoryController');
//User category
Route::resource('/usercategories', 'UsercategoryController');
//notification category
Route::resource('/notificationcategories', 'NotificationcategoryController');