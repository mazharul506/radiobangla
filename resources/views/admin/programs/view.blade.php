@extends('admin.layouts.app')

@section('view')
    <section class="container">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading" style="text-align:left;">
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <h4><b>Program Detail</b></h4>
                    </div>
                    <div class="col-md-8 col-xs-8">
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <a href="#" id="print" class="pull-right"><span class="fa fa-2x fa-print" aria-hidden="true"></span></a>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div id="printable">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="#" class="thumbnail">
                                @if(isset($program->picture) && !empty($program->picture))
                                    <img src="{{asset("uploads/programs/$program->picture")}}" class="img-responsive" alt="ship">
                                @else
                                    <img src="{{asset("uploads/programs/default.gif")}}" class="img-responsive" alt="ship">
                                @endif
                            </a>
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-7">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <tbody>
                                    <tr>
                                        <th class="col-md-3 list_th_right">Program Title</th>
                                        <td class="col-md-9">{{ $program->title }}</td>
                                    </tr>


                                    <tr>
                                        <th class="col-md-3 list_th_right">Short Description</th>
                                        <td class="col-md-9">{{ $program->short_description }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-md-3 list_th_right">Description</th>
                                        <td class="col-md-9">{!! $program->htmlized_description !!}</td>
                                    </tr>

                                    <tr>
                                        <th class="col-md-3 list_th_right">Program Type</th>
                                        <td class="col-md-9">{{ $program->type }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-md-3 list_th_right">Program Content</th>
                                        <td class="col-md-9">Not found</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>{{--end of table responsive--}}
                        </div>
                    </div>{{--end of row--}}
                </div>
                {{--<div class="row pull-right">--}}
                    {{--<a href="{{ url("/programs/$program->id/edit") }}" ><button class="btn">Edit</button></a>--}}
                    {{--<a href="{{ url("/programs") }}" ><button class="btn">Cancel</button></a>--}}
                {{--</div>--}}
                <div class="pull-right">
                    <a href="{{ url("/programs/$program->id/edit") }}" ><button type="button" class="btn">Edit</button></a>
                    <a href="{{ url("/programs") }}" ><button type="button" class="btn">Close</button></a>
                </div>
            </div> {{--end of panel body--}}
        </div>
    </section>
@endsection

@push('css')
<link href="{{asset('default/css/view_program_detail.css')}}"  rel="stylesheet">
@endpush

@push('scripts')
<script src="{{asset('printMe/jquery-printme.js')}}"></script>
<script>
    $("#print").click(function(){
        $("#printable").printMe({ "path": "{{asset('printMe/bootstrap.min.css')}}", "title": " " });
    });
</script>
@endpush

