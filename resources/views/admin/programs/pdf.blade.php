@if(isset($program->picture) && !empty($program->picture))
    <img src='{{asset("uploads/programs/$program->picture")}}'  alt="Program Picture" />
@else
    <img src='{{asset("uploads/programs/default.gif")}}'  alt="Program Picture" />
@endif
<table >
    <tbody>
    <tr>
        <th>Program Title :</th>
        <td>{{ $program->title }}</td>
    </tr>

    <tr>
        <th>Short Description :</th>
        <td>{{ $program->short_description }}</td>
    </tr>
    <tr>
        <th>HTMLize Description :</th>
        <td>{!! $program->htmlized_description !!}</td>
    </tr>
    <tr>
        <th>Program Type :</th>
        <td>{{ $program->type }}</td>
    </tr>
    <tr>
        <th>Content :</th>
        <td>{{ $program->content }}</td>
    </tr>
    </tbody>
</table>