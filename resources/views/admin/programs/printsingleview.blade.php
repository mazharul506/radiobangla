<script>
    window.print();
</script>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>RBNY</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/program/css/bootstrap.min.css')}}"  rel="stylesheet">
    <link href="{{asset('assets/program/css/view_program_detail.css')}}"  rel="stylesheet">
    <link href="{{asset('assets/program/css/font-awesome.min.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="prg_view">
    <section class="container">
        <div class="card card-outline">
            <div class="card-header">
                <h4 class="pull-left">View Program</h4>
            </div>
            <div class="row">
                <div id="printable">
                    <div class="col-md-3">
                        <figure class="figure">
                            <img src='{{asset("uploads/programs/$program->picture")}}' class="img-circle" alt="xorent" />
                            <figcaption class="figure-caption text-xs-center">XORENT</figcaption>
                        </figure>
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-7">
                        <table class="table table-striped table-sm">
                            <tbody>
                            <tr>
                                <th scope="row" class="btn text-right">Program Title :</th>
                                <td>{{ $program->title }}</td>
                            </tr>
                            <tr>
                                <th scope="row" class="btn text-right">Program Description :</th>
                                <td>{!! $program->htmlized_description !!}</td>
                            </tr>
                            <tr>
                                <th scope="row" class="btn text-right">Short Description :</th>
                                <td>{{ $program->short_description }}</td>
                            </tr>
                            <tr>
                                <th scope="row" class="btn text-right">Program Type :</th>
                                <td>{{ $program->type }}</td>
                            </tr>
                            <tr>
                                <th scope="row" class="btn text-right">Content :</th>
                                <td>{{ $program->content }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </section>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('printMe/jquery-printme.js')}}"></script>


</body>
</html>

