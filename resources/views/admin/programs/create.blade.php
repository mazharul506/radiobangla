@extends('admin.layouts.app')

@section('create_form')
    <section class="container">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading" style="text-align:left;"><h4>Create Program</h4></div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="panel-body">
                {!! Form::open(['url' => 'programs/store', 'method'=>'post', 'files' => true,'class'=>'form-horizontal form_program_create']) !!}
                <div class="form-group row">
                    {{ Form::label('title', 'Program Title', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::text('title', null,['class'=>'form-control', 'placeholder'=>'Enter Program Title', 'required'=>'required']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('short_description', 'Short Description', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::textarea('short_description', null,['class'=>'form-control','rows'=>'3']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('title', 'Htmlized Description', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::textarea('htmlized_description', null,['class'=>'form-control','id'=>'htmlized_description']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('type', 'Program Type', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        <label class="radio-inline pull-left">
                        {{ Form::radio('type', 'Live') }}Live &nbsp;
                        </label>
                        <label class="radio-inline pull-left">

                        {{ Form::radio('type', 'Recorded', true) }}Recorded
                        </label>
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('picture', 'Picture', array('class' => 'col-md-2 col-xs-4 col-form-label custom-file')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::file('picture', null,['id'=>'Picture','class'=>'col-form-label custom-file-input']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('content', 'Content', array('id'=>'Picture','class' =>'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::file('content', null,['id'=>'Picture','class'=>'col-form-label custom-file-input']) !!}
                    </div>
                </div>
                <div>
                    {{ Form::button('Save', array('type'=>'submit','name'=>'save','class'=>'btn')) }}
                    {{ Form::button('Save & Edit', array('type'=>'submit','name'=>'save_and_edit','class'=>'btn')) }}
                    {{ Form::button('Reset', array('type'=>'reset','class'=>'btn')) }}
                </div>
                <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
                <script>
                    CKEDITOR.replace( 'htmlized_description' );
                </script>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection



