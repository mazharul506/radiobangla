@extends('admin.layouts.app')

@section('create_form')
<section class="container">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading" style="text-align:left;">
            <h4><b>Program</b></h4>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="panel-body">
            {!! Form::open(['url' => 'programs/'.$program->id.'/update', 'method'=>'post', 'files' => true,'class'=>'form-horizontal form_program_create']) !!}
            <div class="form-group row">
                {{ Form::label('title', 'Program Title', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::text('title', $program->title,['class'=>'form-control', 'placeholder'=>'Enter Program Title', 'required'=>'required']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('short_description', 'Short Description', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::textarea('short_description', $program->short_description,['class'=>'form-control','rows'=>'3']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('title', 'Htmlized Description', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::textarea('htmlized_description', $program->htmlized_description,['class'=>'form-control','id'=>'htmlized_description']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('type', 'Program Type', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    @if($program->type == 'Live')
                        <label class="radio-inline pull-left">
                        {{ Form::radio('type', 'Live', true) }}Live &nbsp;
                        </label>
                        <label class="radio-inline pull-left">
                        {{ Form::radio('type', 'Recorded') }}Recorded
                        </label>
                    @elseif($program->type == 'Recorded')
                        <label class="radio-inline pull-left">
                        {{ Form::radio('type', 'Live') }}Live
                        </label>
                        <label class="radio-inline pull-left">&nbsp;
                        {{ Form::radio('type', 'Recorded', true) }}Recorded
                        </label>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('type', 'Previous Picture', array('class' => 'col-xs-2 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    @if(isset($program->picture) && !empty($program->picture))
                        <img src='{{asset("uploads/programs/$program->picture")}}' class="img-thumbnail" alt="{!! $program->title." Featured Image"   !!}" style="width: 100%; height: 150px;" />
                    @else
                        <img src='{{asset("uploads/programs/default.gif")}}' class="img-thumbnail" alt="{!! $program->title." Featured Image"   !!}" style="width: 100%; height: 150px;" />
                    @endif
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('picture', 'Picture', array('class' => 'col-md-2 col-xs-4 col-form-label custom-file')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::file('picture', null,['id'=>'Picture','class'=>'col-form-label custom-file-input']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('content', 'Content', array('id'=>'Picture','class' =>'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::file('content', null,['id'=>'Picture','class'=>'col-form-label custom-file-input']) !!}
                </div>
            </div>
            <div>
                {{ Form::button('Update', array('type'=>'submit','name'=>'update','class'=>'btn')) }}
                <a href="{{ url("/programs") }}" ><button type="button" class="btn">Close</button></a>
            </div>
            <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
            <script>
                CKEDITOR.replace( 'htmlized_description' );
            </script>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection



