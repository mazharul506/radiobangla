<table border="1" cellpadding="5" style='font-family: "Helvetica Neue",Helvetica,Arial,sans-serif'>
    <caption>Programs list</caption>
    <thead>
        <tr>
            <th>SL</th>
            <th>Title</th>
            <th>Short Description</th>
            <th>Status</th>
            <th>Type</th>
        </tr>
    </thead>
    <tbody>
        <?php $sl=1 ?>
        @foreach($programs as $program)
            <tr>
                <th>{{ $sl++ }}</th>
                <td>{{$program->title  }}</td>
                <td>{{$program->short_description  }}</td>
                <td>
                    @if($program->status == 0)
                        {{ "Draft" }}
                    @elseif($program->status == 1)
                        {{ "Confirmed" }}
                    @elseif($program->status == 2)
                        {{ "Published" }}
                    @endif
                </td>
                <td>{{$program->type  }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
