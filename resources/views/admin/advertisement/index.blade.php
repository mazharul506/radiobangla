@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Advertisement</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/advertisement/create') }}" class="btn btn-primary btn-xs" title="Add New Advertisement"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th><th> Category Id </th><th> Title </th><th> Short Description </th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($advertisement as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->category_id }}</td><td>{{ $item->title }}</td><td>{{ $item->short_description }}</td>
                                        <td>
                                            <a href="{{ url('/admin/advertisement/' . $item->id) }}" class="btn btn-success btn-xs" title="View Advertisement"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url('/admin/advertisement/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Advertisement"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/advertisement', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Advertisement" />', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Advertisement',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $advertisement->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection