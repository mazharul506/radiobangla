@extends('admin.layouts.app')

@section('rj_profile_view')
    <section class="container">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading" style="text-align:left;">
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <h4><b>Detail Of {{ $rj->name }}</b></h4>
                    </div>
                    <div class="col-md-8 col-xs-8">
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <a href="#" id="print" class="pull-right"><span class="fa fa-2x fa-print" aria-hidden="true"></span></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="printable">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="#" class="thumbnail">
                                @if(isset($rj->picture) && !empty($rj->picture))
                                    <img src="{{asset("uploads/rj-profiles/$rj->picture")}}" class="img-responsive" alt="ship">
                                @else
                                    <img src="{{asset("uploads/rj-profiles/default.jpg")}}" class="img-responsive" alt="ship">
                                @endif
                            </a>
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-7">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <tbody>
                                    <tr>
                                        <th class="col-md-3 list_th_right">Name</th>
                                        <td class="col-md-9">{{ $rj->name }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-md-3 list_th_right">ID</th>
                                        <td class="col-md-9">{{ "00".$rj->id }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-md-3 list_th_right">Email</th>
                                        <td class="col-md-9">{{ $rj->email }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-md-3 list_th_right">Contact</th>
                                        <td class="col-md-9">{{ $rj->contact }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-md-3 list_th_right">Birthday</th>
                                        <td class="col-md-9">{{ $rj->date_of_birth }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-md-3 list_th_right">Favourite Music</th>
                                        <td class="col-md-9">{{ $rj->favourite_music }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-md-3 list_th_right">Tv Shows</th>
                                        <td class="col-md-9">{{ $rj->tv_shows }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-md-3 list_th_right">Favourite Palces to Shop</th>
                                        <td class="col-md-9">{{ $rj->favourite_places_to_shop }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-md-3 list_th_right">Happy Place</th>
                                        <td class="col-md-9">{{ $rj->happy_place }}</td>
                                    </tr>
                                    <tr>
                                        <th class="col-md-3 list_th_right">For Fun</th>
                                        <td class="col-md-9">{{ $rj->for_fun }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pull-right">
                    <?php $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>
                    <a href="{{ url(rtrim($url,"/view")."/edit") }}" ><button type="button" class="btn">Edit</button></a>
                    <a href="{{ url(rtrim($url,"/view")."/") }}" ><button type="button" class="btn">Close</button></a>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="{{asset('printMe/jquery-printme.js')}}"></script>
<script>
    $("#print").click(function(){
        $("#printable").printMe({ "path": "{{asset('printMe/bootstrap.min.css')}}", "title": " " });
    });
</script>
@endpush
