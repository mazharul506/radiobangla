@extends('admin.layouts.app')

@section('rj_login')
    <section class="container">
        <div class="row">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><b>Sign In to Host</b></h3>
                        </div>
                        <div class="panel-body">
                            @if(Session::has('msg'))
                                <div class="alert alert-success">
                                    {{ Session::get('msg') }}
                                </div>
                            @endif
                            <div class="thumbnail">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="caption">
                                    {!! Form::open(['url' => 'rj-login-process', 'method'=>'post']) !!}
                                        <div class="form-group">
                                            {{ Form::label('email', 'Email') }}
                                            {!! Form::email('email', null,['class'=>'form-control','required'=>'required','placeholder'=>'example@domain.com']) !!}
                                        </div>
                                        <div class="form-group">
                                            {{ Form::label('password', 'Password') }}
                                            {!! Form::password('password', null,['class'=>'form-control','required'=>'required','placeholder'=>'password']) !!}
                                        </div>
                                        <button type="submit" class="btn btn-default">Logn In</button>
                                    {{ Form::close() }}
                                    {{--<form>--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="exampleInputEmail1">Email address</label>--}}
                                            {{--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group">--}}
                                            {{--<label for="exampleInputPassword1">Password</label>--}}
                                            {{--<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>--}}
                                        {{--</div>--}}
                                        {{--<button type="submit" class="btn btn-default">Logn In</button>--}}
                                    {{--</form>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                </div>
            </div>
        </div>
    </section>
@endsection