@push('css')
<!-- Jquery-ui CSS -->
<link href="{{asset('default/css/jquery-ui.css')}}" rel="stylesheet">
@endpush

@extends('admin.layouts.app')

@section('rj_profile_edit_form')
    <section class="container">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading" style="text-align:left;"><h4><b>Edit Host Profile</b></h4></div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="panel-body">
                <?php $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>
                {!! Form::open(['url' => rtrim($url,"/edit").'/update', 'method'=>'post', 'files' => true,'class'=>'form-horizontal form_program_create']) !!}
                <div class="form-group row">
                    {{ Form::label('type', 'Previous Picture', array('class' => 'col-xs-2 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        <div style="float: left">
                            @if(isset($rj->picture) && !empty($rj->picture))
                                <img src='{{asset("uploads/rj-profiles/$rj->picture")}}' class="img-thumbnail" alt="picture"/>
                            @else
                                <img src='{{asset("uploads/rj-profiles/default.jpeg")}}' class="img-thumbnail" alt="picture"/>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('picture', 'Profile Picture', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::file('picture', null,['id'=>'Picture','class'=>'col-form-label custom-file-input']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('email', 'Email', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::text('email', $rj->email,['class'=>'col-form-label form-control']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('contact', 'Contact', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::text('contact', $rj->contact,['class'=>'col-form-label form-control','maxlength'=>'11','minlength'=>'11']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('date_of_birth', 'DateOfBirth', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::date('date_of_birth', $rj->date_of_birth,['id'=>'dfdf','class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('favourite_music', 'Favourite Music', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::text('favourite_music', $rj->favourite_music,['class'=>'col-form-label form-control']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('tv_shows', 'Tv Shows', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::text('tv_shows', $rj->tv_shows,['class'=>'col-form-label form-control']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('favourite_places_to_shop', 'Favourite Palces to Shop', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::text('favourite_places_to_shop', $rj->favourite_places_to_shop,['class'=>'col-form-label form-control']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('happy_place', 'Happy Place', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::text('happy_place', $rj->happy_place,['class'=>'col-form-label form-control']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('for_fun', 'For Fun', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::text('for_fun', $rj->for_fun,['class'=>'col-form-label form-control']) !!}
                    </div>
                </div>
                <div>
                    {{ Form::button('Update', array('type'=>'submit','name'=>'update','class'=>'btn')) }}
                    <a href="{{ url(rtrim($url,"/edit")) }}" ><button type="button" class="btn">Close</button></a>
                </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#rj_dob" ).datepicker();
    } );
</script>
@endpush
