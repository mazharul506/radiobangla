@extends('admin.layouts.app')

@section('rj_profile')
    <section class="container">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading" style="text-align:left;">
                <h4><b>Host Detail</b></h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div>
                            <a href="#">
                                <img src="{{asset("uploads/rj-profiles/$rj->picture")}}" class=" img-circle img-responsive" alt="ship">
                            </a>
                            <p class="text-center host_profile_button">
                                <?php  $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']  ?>
                                <a href="{{ url($url."/view") }}" class="btn">View Profile</a>
                                <a href="{{ url($url."/edit") }}" class="btn">Edit Profile</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-7">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <tbody>
                                <tr>
                                    <th class="col-md-4 text-center">Program Title</th>
                                    <th class="col-md-3 text-center">Date</th>
                                    <th class="col-md-2 text-center">Start Time</th>
                                    <th class="col-md-2 text-center">End Time</th>
                                </tr>
                                <tr>
                                    <td class="col-md-4 text-center">{{ $rj->name  }}</td>
                                    <td class="col-md-3 text-center">18-01-2017</td>
                                    <td class="col-md-2 text-center">09.00 am</td>
                                    <td class="col-md-2 text-center">11.00 am</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="http://listjs.com/assets/javascripts/list.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/list.pagination.js/0.1.1/list.pagination.min.js"></script>
<script src="{{asset('default/js/raphael.min.js')}}"></script>
<script src="{{asset('default/js/morris.min.js')}}"></script>
<script src="{{asset('default/js/morris-data.js')}}"></script>

<script type="text/javascript" src="{{asset('default/js/jquery-latest.js')}}"></script>
<script type="text/javascript" src="{{asset('default/js/sorttable.js')}}"></script>

<script>
    //pagination
    var dataList = new List('test-list', {
        valueNames: ['name','id','title', 'email', 'date_of_birth',],
        page: 15,
        plugins: [ ListPagination({}) ]
    });
</script>
@endpush