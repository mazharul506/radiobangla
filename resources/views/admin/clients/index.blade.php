@extends('admin.layouts.app')

@section('clients')
    <div class="container">
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                {{ Session::get('flash_message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Clients</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/clients/create') }}" class="btn btn-primary btn-xs" title="Add New Client"><span class="glyphicon glyphicon-plus" aria-hidden="true"/></a>
                        <a href="{{ url('/admin/clients/downloadExcel') }}" class="btn btn-primary btn-xs" title="Export To Excel"><span class="fa fa-2x fa-file-excel-o" aria-hidden="true"/></a>
                        <br/>
                        <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 20px;" action="{{ URL::to('admin/clients/importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-3">
                                    <input type="file" name="import_file" /><a href="{{ asset('downloads/clientImport.xlsx')  }}" class="glyphicon glyphicon-download">file</a>
                                    {{ csrf_field() }}
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-primary">Import CSV or Excel File</button>
                                </div>
                            </div>
                            <br/>
                        </form>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th><th>Title</th><th> Types Of Company </th><th> Address </th><th> State </th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($clients as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->types_of_company }}</td><td>{{ $item->address }}</td><td>{{ $item->state }}</td>
                                        <td>
                                            <a href="{{ url('/admin/clients/' . $item->id) }}" class="btn btn-success btn-xs" title="View Client"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"/></a>
                                            <a href="{{ url('/admin/clients/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Client"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/clients', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete Client" />', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-xs',
                                                        'title' => 'Delete Client',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $clients->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection