<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('types_of_company') ? 'has-error' : ''}}">
    {!! Form::label('types_of_company', 'Types Of Company', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('types_of_company', ['Benefit corporation', 'C corporation', 'Limited liability company (LLC) Low-profit LLC', 'Series LLC', 'Limited liability limited partnership (LLLP)'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('types_of_company', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', 'Address', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address', null, ['class' => 'form-control']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
    {!! Form::label('state', 'State', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('state', null, ['class' => 'form-control']) !!}
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', 'Country', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('country', null, ['class' => 'form-control']) !!}
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('zip_code') ? 'has-error' : ''}}">
    {!! Form::label('zip_code', 'Zip Code', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('zip_code', null, ['class' => 'form-control']) !!}
        {!! $errors->first('zip_code', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('phone_1') ? 'has-error' : ''}}">
    {!! Form::label('phone_1', 'Phone 1', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('phone_1', null, ['class' => 'form-control']) !!}
        {!! $errors->first('phone_1', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('phone_2') ? 'has-error' : ''}}">
    {!! Form::label('phone_2', 'Phone 2', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('phone_2', null, ['class' => 'form-control']) !!}
        {!! $errors->first('phone_2', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
    {!! Form::label('website', 'Website', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('website', null, ['class' => 'form-control']) !!}
        {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
    {!! Form::label('contact_name', 'Contact Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contact_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('contact_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('contact_designation') ? 'has-error' : ''}}">
    {!! Form::label('contact_designation', 'Contact Designation', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contact_designation', null, ['class' => 'form-control']) !!}
        {!! $errors->first('contact_designation', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('contact_phone_1') ? 'has-error' : ''}}">
    {!! Form::label('contact_phone_1', 'Contact Phone 1', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contact_phone_1', null, ['class' => 'form-control']) !!}
        {!! $errors->first('contact_phone_1', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('contact_phone_2') ? 'has-error' : ''}}">
    {!! Form::label('contact_phone_2', 'Contact Phone 2', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contact_phone_2', null, ['class' => 'form-control']) !!}
        {!! $errors->first('contact_phone_2', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
    {!! Form::label('contact_email', 'Contact Email', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contact_email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('contact_email', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('assigned_person') ? 'has-error' : ''}}">
    {!! Form::label('assigned_person', 'Assigned Person', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('assigned_person', null, ['class' => 'form-control']) !!}
        {!! $errors->first('assigned_person', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('attachment') ? 'has-error' : ''}}">
    {!! Form::label('attachment', 'Attachment', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('attachment', null, ['class' => 'form-control']) !!}
        {!! $errors->first('attachment', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>