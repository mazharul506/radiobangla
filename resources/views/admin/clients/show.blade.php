@extends('admin.layouts.app')

@section('client_view')
    <section class="container">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading" style="text-align:left;">
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <h4><b>Client Detail</b></h4>
                    </div>
                    <div class="col-md-8 col-xs-8">
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <a href="#" id="print" class="pull-right"><span class="fa fa-2x fa-print" aria-hidden="true"></span></a>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <a href="{{ url('admin/clients/' . $client->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Client"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/clients', $client->id],
                    'style' => 'display:inline'
                ]) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Client',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
                {!! Form::close() !!}
                <div id="printable">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <tbody>
                                        <tr>
                                            <th>ID</th><td>{{ $client->id }}</td>
                                        </tr>
                                        <tr>
                                            <th>Title</th><td>{{ $client->title }}</td>
                                        </tr>
                                        <tr>
                                            <th> Types Of Company </th><td> {{ $client->types_of_company }} </td>
                                        </tr>
                                        <tr><th> Address </th><td> {{ $client->address }} </td></tr>
                                        <tr><th> State </th><td> {{ $client->state }} </td></tr>
                                        <tr><th> Country </th><td> {{ $client->country }} </td></tr>
                                        <tr><th> Zip Code </th><td> {{ $client->zip_code }} </td></tr>
                                        <tr><th> Email </th><td> {{ $client->email }} </td></tr>
                                        <tr><th> Phone 1 </th><td> {{ $client->phone_1 }} </td></tr>
                                        <tr><th> Phone 2 </th><td> {{ $client->phone_2 }} </td></tr>
                                        <tr><th> Website </th><td> {{ $client->website }} </td></tr>
                                        <tr><th> Contact Name </th><td> {{ $client->contact_name }} </td></tr>
                                        <tr><th> Contact Designation </th><td> {{ $client->contact_designation }} </td></tr>
                                        <tr><th> Contact Phone 1 </th><td> {{ $client->contact_phone_1 }} </td></tr>
                                        <tr><th> Contact Phone 2 </th><td> {{ $client->contact_phone_2 }} </td></tr>
                                        <tr><th> Contact Email </th><td> {{ $client->contact_email }} </td></tr>
                                        <tr><th> Assigned Person </th><td> {{ $client->assigned_person }} </td></tr>
                                        <tr><th> Attachment </th><td> {{ $client->attachment }} </td></tr>
                                    </tbody>
                                </table>
                            </div>{{--end of table responsive--}}
                        </div>
                    </div>{{--end of row--}}
                </div>
                {{--<div class="row pull-right">--}}
                {{--<a href="{{ url("/programs/$program->id/edit") }}" ><button class="btn">Edit</button></a>--}}
                {{--<a href="{{ url("/programs") }}" ><button class="btn">Cancel</button></a>--}}
                {{--</div>--}}
                <div class="pull-right">
                    {{--<a href="{{ url("/programs/$program->id/edit") }}" ><button type="button" class="btn">Edit</button></a>--}}
                    {{--<a href="{{ url("/programs") }}" ><button type="button" class="btn">Close</button></a>--}}
                </div>
            </div> {{--end of panel body--}}
        </div>
    </section>

@endsection