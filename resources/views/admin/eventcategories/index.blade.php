@extends('admin.layouts.app')

@section('content')
<div id="list">
    <section class="container" id="test-list">
        @if(Session::has('msg'))
            <div class="alert alert-success">
                {{ Session::get('msg') }}
            </div>
        @endif
        <div class="panel panel-default" >
            <!-- Default panel contents -->
            <div class="panel-heading" style="text-align:left;">
                <h4><b>Event Category</b></h4>
                <div class="row">
                    <div class="col-md-4">
                        <form class="navbar-form" style="margin-left:-15px;" >
                            <div class="form-group">
                                <input type="search" class="form-control search" placeholder="Search">
                            </div>
                            <a href="{{ url("/eventcategories/create")}}" class="btn btn-default">+ ADD NEW </a>
                        </form>
                    </div>
                    <div class="col-md-6"></div>
                    <div class="col-md-2">

                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-hover table-striped tablesorter sortable">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Category Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            <?php $sl=1 ?>
                            @foreach($eventcategories as $eventcategory)
                                <tr>
                                    <th scope="row">{{ $sl++ }}</th>
                                    <td class="name title">{{$eventcategory->name  }}</td>

                                    <td>
                                        <a href="{{ url("/eventcategories/$eventcategory->id") }}"><i class="fa fa-window-maximize" title="View" aria-hidden="true"></i></a>
                                        <a href="{{ url("/eventcategories/$eventcategory->id/edit") }}"><i class="fa fa-pencil-square" title="Edit" aria-hidden="true"></i></a>
                                        <a href="#" onclick="del({{$eventcategory->id}})" class="delete" data-confirm="Are you sure to delete this item?"><i class="fa fa-window-close" title="Delete" aria-hidden="true"></i></a>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-2">
                        <ul class="pagination push_page_num">
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="#">1<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link" href="#">2<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                     </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('scripts')
<script src="http://listjs.com/assets/javascripts/list.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/list.pagination.js/0.1.1/list.pagination.min.js"></script>
<script src="{{asset('default/js/raphael.min.js')}}"></script>
<script src="{{asset('default/js/morris.min.js')}}"></script>
<script src="{{asset('default/js/morris-data.js')}}"></script>

<script type="text/javascript" src="{{asset('default/js/jquery-latest.js')}}"></script>
<script type="text/javascript" src="{{asset('default/js/sorttable.js')}}"></script>

<script>
    function del($id) {
        if(confirm("Do you want to delete?")){
            var name = prompt("Please enter event category_name", "");
            if (name != null && name != '') {
                window.location.href = '/eventcategories/'+$id+'/'+name+'/delete';
            }else{
                alert("category name should not be empty");
            }
            return true;
        }else{
            alert('Cancelled');
        }
    }
    //pagination
    var dataList = new List('test-list', {
        valueNames: ['name', 'title', 'description', 'status', 'type',],
        page: 15,
        plugins: [ ListPagination({}) ]
    });
</script>
@endpush