@extends('admin.layouts.app')

@section('create_form')
    <section class="container">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading" style="text-align:left;"><h4>Create Program</h4></div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="panel-body">
                {!! Form::open(['url' => 'eventcategories/store', 'method'=>'post', 'files' => true,'class'=>'form-horizontal form_program_create']) !!}
                <div class="form-group row">
                    {{ Form::label('name', 'Event Category Name', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                    <div class="col-md-10 col-xs-8">
                        {!! Form::text('name', null,['class'=>'form-control', 'placeholder'=>'Enter Program Title', 'required'=>'required']) !!}
                    </div>
                </div>

                <div>
                    {{ Form::button('Save', array('type'=>'submit','name'=>'save','class'=>'btn')) }}
                    {{ Form::button('Save & Edit', array('type'=>'submit','name'=>'save_and_edit','class'=>'btn')) }}
                    {{ Form::button('Reset', array('type'=>'reset','class'=>'btn')) }}
                </div>
                <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
                <script>
                    CKEDITOR.replace( 'htmlized_description' );
                </script>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection



