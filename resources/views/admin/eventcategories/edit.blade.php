@extends('admin.layouts.app')

@section('create_form')
<section class="container">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading" style="text-align:left;">
            <h4><b>Event Category</b></h4>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="panel-body">
            {!! Form::open(['url' => 'eventcategories/'.$eventcategory->id.'/update', 'method'=>'post', 'files' => true,'class'=>'form-horizontal form_program_create']) !!}
            <div class="form-group row">
                {{ Form::label('name', 'Event Category Title', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::text('name',$eventcategory->name,['class'=>'form-control', 'placeholder'=>'Enter Event Category Title', 'required'=>'required']) !!}
                </div>
            </div>
            <div>
                {{ Form::button('Update', array('type'=>'submit','name'=>'update','class'=>'btn')) }}
                {{ Form::button('Reset', array('type'=>'reset','class'=>'btn')) }}
            </div>
            <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
            <script>
                CKEDITOR.replace( 'htmlized_description' );
            </script>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection



