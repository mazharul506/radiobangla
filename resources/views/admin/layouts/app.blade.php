<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>radiobanglaNY</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('default/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('default/css/style.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('default/css/rbny-admin.css')}}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{asset('default/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="{{asset('default/css/morris.css')}}" rel="stylesheet">
    {{--full calender--}}
    <link href="{{asset('fullcalendar/fullcalendar.min.css')}}" rel='stylesheet' />
    <link href="{{asset('fullcalendar/fullcalendar.print.min.css')}}" rel='stylesheet' media='print' />
    <script src="{{asset('default/js/jquery.js')}}"></script>
    <script src="{{asset('fullcalendar/lib/moment.min.js')}}"></script>
    <script src="{{asset('fullcalendar/lib/jquery-ui.min.js')}}"></script>
    <script src="{{asset('fullcalendar/fullcalendar.min.js')}}"></script>
    @stack('css')
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.default/default/js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Radio Bangla NY</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {{--<a class="navbar-brand" href="index.html">Radio Bangla NY</a>--}}
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li><a href="{{ url("/rj-login") }}" ><button class="btn">Host Login</button></a></li>
                <li><a href="{{ url("/rj-profiles") }}" ><button class="btn">Hosts</button></a></li>
                <li><a href="{{ url("/programs") }}" ><button class="btn">Programs</button></a></li>
                <li><a href="{{ url("/schedule") }}" ><button class="btn">Schedules</button></a></li>
                <li><a href="{{ url("/admin/clients") }}" ><button class="btn">Clients</button></a></li>
                <li><a href="{{ url("/admin/group") }}" ><button class="btn">Groups</button></a></li>
                <li><a href="{{ url("/admin/user-profile") }}" ><button class="btn">Users</button></a></li>
                <li><a href="{{ url("/admin/advertisement") }}" ><button class="btn">Advertisement</button></a></li>
                <li><a href="{{ url("/events") }}" ><button class="btn">Event List</button></a></li>
                <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><button class="btn">Category List</button></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/eventcategories">Event Categoryt</a>
                        </li>
                        <li>
                            <a href="/programcategories">Program Category</a>
                        </li>
                        <li>
                            <a href="/guestcategories">Guest Category</a>
                        </li>

                        <li>
                            <a href="/groupcategories">Group Category</a>
                        </li>
                        <li>
                            <a href="/schedulecategories">Schedule Category</a>
                        </li>
                        <li>
                            <a href="/advertisementcategories">Advertisement Category</a>
                        </li>
                        <li>
                            <a href="/archivecategories">Archive Category</a>
                        </li>
                        <li>
                            <a href="/companycategories">Company Category</a>
                        </li>
                        <li>
                            <a href="/articlecategories">Article Category</a>
                        </li>
                        <li>
                            <a href="/festivalcategories">Festival Category</a>
                        </li>
                        <li>
                            <a href="/contestcategories">Contest Category</a>
                        </li>
                        <li>
                            <a href="/usercategories">User Category</a>
                        </li>
                        <li>
                            <a href="/notificationcategories">Notification Category</a>
                        </li>
                    </ul>
                </li>
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>--}}
                {{--</li>--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>--}}
                {{--</li>--}}
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
 {{--program--}}
    @yield('content')
    @yield('create_form')
    @yield('edit_form')
    @yield('view')

    {{--rj management--}}
    {{--rj head--}}
    @yield('rj_content')
    @yield('rj_create_form')
    @yield('rj_view')
    @yield('rj_edit_form')

    {{--rj--}}
    @yield('rj_login')
    @yield('rj_profile')
    @yield('rj_profile_view')
    @yield('rj_profile_edit_form')

    {{--schedule--}}
    @yield('schedule')

    {{--client/company--}}
    @yield('clients')
    @yield('client_create')
    @yield('client_edit')
    @yield('client_view')

    <!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('default/js/bootstrap.min.js')}}"></script>
    @stack('scripts')

</body>
</html>
