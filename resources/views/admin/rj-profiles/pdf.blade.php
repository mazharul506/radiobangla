
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        table {
            border-collapse: collapse;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            width: 100%;
        }
        table, td, th {
            border: 1px solid black;
            padding: 3px;
        }
    </style>
</head>
<body>
    <div style="height: 200px; width: 200px; margin: 0 auto">
        @if(isset($rj->picture) && !empty($rj->picture))
            <img src='{{asset("uploads/rj-profiles/$rj->picture")}}' height="200px" width="200px" alt="RJ Picture" />
        @else
            <img src='{{asset("uploads/rj-profiles/default.gif")}}' height="200px" width="200px" alt="RJ Picture" />
        @endif
    </div>
    <table>
        <tbody>
            <tr>
                <th >Host Name</th>
                <td>{{ $rj->name }}</td>
            </tr>
            <tr>
                <th >Email</th>
                <td>{{ $rj->email }}</td>
            </tr>
            <tr>
                <th >Birthday</th>
                <td>{{ $rj->date_of_birth }}</td>
            </tr>
            <tr>
                <th >Favourite Music</th>
                <td>{{ $rj->favourite_music }}</td>
            </tr>
            <tr>
                <th >Tv Shows</th>
                <td>{{ $rj->tv_shows }}</td>
            </tr>
            <tr>
                <th >Favourite Palces to Shop</th>
                <td>{{ $rj->favourite_places_to_shop }}</td>
            </tr>
            <tr>
                <th >Happy Place</th>
                <td>{{ $rj->happy_place }}</td>
            </tr>
            <tr>
                <th >For Fun</th>
                <td>{{ $rj->for_fun }}</td>
            </tr>
        </tbody>
    </table>
</body>
</html>