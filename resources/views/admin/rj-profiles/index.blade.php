@extends('admin.layouts.app')

@section('rj_content')
<div id="list">
    <section class="container" id="test-list">
        @if(Session::has('msg'))
            <div class="alert alert-success">
                {{ Session::get('msg') }}
            </div>
        @endif
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading" style="text-align:left;">
                <h4><b>Host List Table</b></h4>
                <div class="row">
                    <div class="col-md-4">
                        <form class="navbar-form" style="margin-left:-15px;" >
                            <div class="form-group">
                                <input type="search" class="form-control search" placeholder="Search">
                            </div>
                            <a href="{{ url("/rj-profiles/create")}}" class="btn btn-default">+ ADD NEW </a>
                        </form>
                    </div>
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-2">
                        <div class="pull-right">
                            <a href="{{url("/rj-profiles/rj-list-pdf-download")}}"><i class="fa fa-2x fa-file-pdf-o" title="Downoad PDF" aria-hidden="true"></i></a>
                            <a href="{{url("rj-profiles/rj-list-excel-download")}}"><i class="fa fa-2x fa-file-excel-o" title="Downoad Excel" aria-hidden="true"></i></a>
                            <a href="{{ url("rj-profiles/rj-list-print/") }}" target="_blank"><i class="fa fa-2x fa-print" title="Print"  aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-hover table-striped tablesorter sortable">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Birthday</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            <?php $sl = 1; ?>
                            @foreach($rjs as $rj)
                                <tr>
                                    <td>{{ $sl++  }}</td>
                                    <td class="name id">{{ "00".$rj->id  }}</td>
                                    <td class="name title">{{ $rj->name  }}</td>
                                    <td class="name email">{{ $rj->email  }}</td>
                                    <td class="name email">{{ $rj->contact  }}</td>
                                    <td class="name date_of_birth">{{ $rj->date_of_birth  }}</td>
                                    <td>
                                        <a href="{{ url("/rj-profiles/$rj->id") }}"><i class="fa fa-window-maximize" title="View" aria-hidden="true"></i></a>
                                        <a href="{{ url("/rj-profiles/$rj->id/edit") }}"><i class="fa fa-pencil-square" title="Edit" aria-hidden="true"></i></a>
                                        <a href="#"  onclick="del({{$rj->id}})"><i class="fa fa-window-close" title="Delete" aria-hidden="true"></i></a>
                                        <a href="{{ url("/rj-profiles/$rj->id/$rj->name/pdf") }}" ><i class="fa fa-file-pdf-o" title="Downoad PDF" aria-hidden="true"></i></a>
                                        <a href="{{ url("/rj-profiles/print/$rj->id") }}" target="_blank" ><i class="fa fa-print" title="Print"  aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-2">
                        <ul class="pagination push_page_num">
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="#">1<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item ">
                                <a class="page-link" href="#">2<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('scripts')
<script src="http://listjs.com/assets/javascripts/list.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/list.pagination.js/0.1.1/list.pagination.min.js"></script>
<script src="{{asset('default/js/raphael.min.js')}}"></script>
<script src="{{asset('default/js/morris.min.js')}}"></script>
<script src="{{asset('default/js/morris-data.js')}}"></script>

<script type="text/javascript" src="{{asset('default/js/jquery-latest.js')}}"></script>
<script type="text/javascript" src="{{asset('default/js/sorttable.js')}}"></script>

<script>
    function del($id) {
        if(confirm("Do you want to delete?")){
            var name = prompt("Please enter rj name", "");
            if (name != null && name != '') {
                window.location.href = '/rj-profiles/'+$id+'/'+name+'/delete';
            }else{
                alert("Host name should not be empty");
            }
            return true;
        }else{
            alert('Cancelled');
        }
    }
    //pagination
    var dataList = new List('test-list', {
        valueNames: ['name','id','title', 'email', 'date_of_birth',],
        page: 15,
        plugins: [ ListPagination({}) ]
    });
</script>
@endpush