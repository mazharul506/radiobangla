<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            border-collapse: collapse;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            width: 100%;
        }
        table, td, th {
            border: 1px solid black;
            padding: 3px;
        }
    </style>
</head>
<body>
    <table>
        <caption>Host list</caption>
        <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Birthday</th>
            </tr>
        </thead>
        <tbody>
            <?php $sl=1 ?>
            @foreach($rjs as $rj)
                <tr>
                    <th>{{ $sl++ }}</th>
                    <td>{{ $rj->id }}</td>
                    <td>{{ $rj->name }}</td>
                    <td>{{ $rj->email }}</td>
                    <td>{{ $rj->date_of_birth }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
