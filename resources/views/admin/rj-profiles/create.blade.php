@push('css')
    <link href="{{asset('default/css/jquery-ui.css')}}" rel="stylesheet">
@endpush

@extends('admin.layouts.app')

@section('rj_create_form')
    <section class="container">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading" style="text-align:left;"><h4><b>Create Host Profile</b></h4></div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="panel-body">
                {!! Form::open(['url' => 'rj-profiles/store', 'method'=>'post', 'files' => true,'class'=>'form-horizontal form_program_create']) !!}
                    <div class="form-group row">
                        {{ Form::label('Name', 'Host Name', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                        <div class="col-md-10 col-xs-8">
                            {!! Form::text('name', null,['class'=>'form-control','required'=>'required']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('picture', 'Profile Picture', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                        <div class="col-md-10 col-xs-8">
                            {!! Form::file('picture', null,['id'=>'Picture','class'=>'col-form-label custom-file-input']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('email', 'Email', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                        <div class="col-md-10 col-xs-8">
                            {!! Form::text('email', null,['class'=>'col-form-label form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('contact', 'Contact', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                        <div class="col-md-10 col-xs-8">
                            {!! Form::text('contact', null,['class'=>'col-form-label form-control','maxlength'=>'11','minlength'=>'11']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('date_of_birth', 'DateOfBirth', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                        <div class="col-md-10 col-xs-8">
                            {!! Form::date('date_of_birth', null,['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('favourite_music', 'Favourite Music', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                        <div class="col-md-10 col-xs-8">
                            {!! Form::text('favourite_music', null,['class'=>'col-form-label form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('tv_shows', 'Tv Shows', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                        <div class="col-md-10 col-xs-8">
                            {!! Form::text('tv_shows', null,['class'=>'col-form-label form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('favourite_places_to_shop', 'Favourite Palces to Shop', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                        <div class="col-md-10 col-xs-8">
                            {!! Form::text('favourite_places_to_shop', null,['class'=>'col-form-label form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('happy_place', 'Happy Place', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                        <div class="col-md-10 col-xs-8">
                            {!! Form::text('happy_place', null,['class'=>'col-form-label form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {{ Form::label('for_fun', 'For Fun', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                        <div class="col-md-10 col-xs-8">
                            {!! Form::text('for_fun', null,['class'=>'col-form-label form-control']) !!}
                        </div>
                    </div>
                    <div>
                        {{ Form::button('Save', array('type'=>'submit','name'=>'save','class'=>'btn')) }}
                        {{ Form::button('Save & Edit', array('type'=>'submit','name'=>'save_and_edit','class'=>'btn')) }}
                        {{ Form::button('Reset', array('type'=>'reset','class'=>'btn')) }}
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#rj_dob" ).datepicker();
        } );
    </script>
@endpush
