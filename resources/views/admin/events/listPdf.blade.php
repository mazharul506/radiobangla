<table border="1" cellpadding="5" style='font-family: "Helvetica Neue",Helvetica,Arial,sans-serif'>
    <caption>Event list</caption>
    <thead>
        <tr>
            <th>SL</th>
            <th>Title</th>
            <th>Short Description</th>
            <th>Status</th>
            <th>Type</th>
        </tr>
    </thead>
    <tbody>
        <?php $sl=1 ?>
        @foreach($events as $event)
            <tr>
                <th>{{ $sl++ }}</th>
                <td>{{$event->title  }}</td>
                <td>{{$event->short_description  }}</td>
                <td>
                    @if($event->status == 0)
                        {{ "Draft" }}
                    @elseif($event->status == 1)
                        {{ "Confirmed" }}
                    @elseif($event->status == 2)
                        {{ "Published" }}
                    @endif
                </td>

                <td>{{$event->type  }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
