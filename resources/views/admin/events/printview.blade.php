<script>
    window.print();
</script>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>RBNY</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/program/css/bootstrap.min.css')}}"  rel="stylesheet">
    <link href="{{asset('assets/program/css/view_program_detail.css')}}"  rel="stylesheet">
    <link href="{{asset('assets/program/css/font-awesome.min.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="prg_view">
    <section class="container">
        <div class="card card-outline">
            <div class="card-header">
                <h4 class="pull-left">Event</h4>
            </div>
            <div class="row">
                <div id="printable">
                    <div class="col-md-3">
                        <figure class="figure">
                            {{--<img src='{{asset("uploads/programs/$program->picture")}}' class="img-circle" alt="xorent" />--}}
                            {{--<figcaption class="figure-caption text-xs-center">XORENT</figcaption>--}}
                        </figure>
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-7">
                        <table class="table table-striped table-sm">

                            <tr>
                                <th >Event Title </th>
                                <th>Short Description </th>
                                <th>Status</th>
                                <th>Event Type </th>
                            </tr>
                            <tr>
                            @foreach($event as $event)

                                    <td>{{ $event->title }}</td>
                                    <td>{{ $event->short_description }}</td>
                                    <td class="name status">
                                        @if($event->status == 0)
                                            {{ "Draft" }}
                                        @elseif($event->status == 1)
                                            {{ "Confirmed" }}
                                        @elseif($event->status == 2)
                                            {{ "Published" }}
                                        @endif
                                    </td>
                                    <td>{{ $event->type }}</td>


                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </section>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('printMe/jquery-printme.js')}}"></script>


</body>
</html>

