@if(isset($event->picture) && !empty($event->picture))
    <img src='{{asset("uploads/events/$event->picture")}}'  alt="Program Picture" />
@else
    <img src='{{asset("uploads/events/default.gif")}}'  alt="Program Picture" />
@endif
<table >
    <tbody>
    <tr>
        <th>Event Title :</th>
        <td>{{ $event->title }}</td>
    </tr>

    <tr>
        <th>Short Description :</th>
        <td>{{ $event->short_description }}</td>
    </tr>
    <tr>
        <th>HTMLize Description :</th>
        <td>{!! $event->htmlized_description !!}</td>
    </tr>
    <tr>
        <th>Event Type :</th>
        <td>{{ $event->type }}</td>
    </tr>
    <tr>
        <th>Content :</th>
        <td>{{ $event->content }}</td>
    </tr>
    </tbody>
</table>