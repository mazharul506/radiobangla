@extends('admin.layouts.app')

@section('create_form')
<section class="container">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading" style="text-align:left;">
            <h4><b>Event</b></h4>
        </div>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="panel-body">
            {!! Form::open(['url' => 'events/'.$event->id.'/update', 'method'=>'post', 'files' => true,'class'=>'form-horizontal form_program_create']) !!}
            <div class="form-group row">

                {{ Form::label('eventcategories_id', 'Event Category', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {{--{!! Form::select('eventcategories_id',$event, null,['class'=>'form-control','placeholder'=>'select...']) !!}--}}
                    {{--{!! Form::select('eventcategories_id',null,['placeholder'=>'select...']) !!}--}}
                    {!! Form::select('eventcategories_id', $eventcategories,isset($event->eventcategories_id) ? $event->eventcategories_id:null,['placeholder'=>'select...','class'=>'form-control']) !!}
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('title', 'Event Title', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::text('title', $event->title,['class'=>'form-control', 'placeholder'=>'Enter Event Title', 'required'=>'required']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('short_description', 'Short Description', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::textarea('short_description', $event->short_description,['class'=>'form-control','rows'=>'3']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('title', 'Htmlized Description', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::textarea('htmlized_description', $event->htmlized_description,['class'=>'form-control','id'=>'htmlized_description']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('date', 'Date', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::date('date',$event->date,['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('start', 'Start Time', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::time('start',$event->start,['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('end', 'End Time', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::time('end', $event->end,['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('type', 'Event Type', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    @if($event->type == 'Paid')
                        <label class="radio-inline pull-left">
                        {{ Form::radio('type', 'Paid', true) }}Paid &nbsp;
                        </label>
                        <label class="radio-inline pull-left">
                        {{ Form::radio('type', 'Free') }}Free
                        </label>
                    @elseif($event->type == 'Free')
                        <label class="radio-inline pull-left">
                        {{ Form::radio('type', 'Paid') }}Paid
                        </label>
                        <label class="radio-inline pull-left">&nbsp;
                        {{ Form::radio('type', 'Free', true) }}Free
                        </label>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('venue', 'Event Venue', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::text('venue',$event->venue,['class'=>'form-control', 'placeholder'=>'Enter Event Venue', 'required'=>'required']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('contact', 'Contact', array('class' => 'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::text('contact', $event->contact,['class'=>'col-form-label form-control','maxlength'=>'11','minlength'=>'11']) !!}
                </div>
            </div>

            <div class="form-group row">
                {{ Form::label('type', 'Previous Picture', array('class' => 'col-xs-2 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    @if(isset($event->picture) && !empty($event->picture))
                        <img src='{{asset("uploads/events/$event->picture")}}' class="img-thumbnail" alt="{!! $event->title." Featured Image"   !!}" style="width: 100%; height: 150px;" />
                    @else
                        <img src='{{asset("uploads/events/default.gif")}}' class="img-thumbnail" alt="{!! $event->title." Featured Image"   !!}" style="width: 100%; height: 150px;" />
                    @endif
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('picture', 'Picture', array('class' => 'col-md-2 col-xs-4 col-form-label custom-file')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::file('picture', null,['id'=>'Picture','class'=>'col-form-label custom-file-input']) !!}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('content', 'Content', array('id'=>'Picture','class' =>'col-md-2 col-xs-4 col-form-label')) }}
                <div class="col-md-10 col-xs-8">
                    {!! Form::file('content', null,['id'=>'Picture','class'=>'col-form-label custom-file-input']) !!}
                </div>
            </div>
            <div>
                {{ Form::button('Update', array('type'=>'submit','name'=>'update','class'=>'btn')) }}
                {{ Form::button('Reset', array('type'=>'reset','class'=>'btn')) }}
            </div>
            <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
            <script>
                CKEDITOR.replace( 'htmlized_description' );
            </script>
            {!! Form::close() !!}
        </div>
    </div>
</section>
@endsection



