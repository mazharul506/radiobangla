<?php

use Illuminate\Database\Seeder;

class RjprofilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Rjprofile::class, 50)->create();
    }
}
