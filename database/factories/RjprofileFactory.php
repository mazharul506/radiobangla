<?php
$factory->define(App\Rjprofile::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->name,
        'picture' => $faker->name(),
        'email' => $faker->email,
        'contact' => $faker->phoneNumber,
        'date_of_birth' => $faker->date(),
        'favourite_music' => $faker->name(),
        'tv_shows' => $faker->name(),
        'favourite_places_to_shop' => $faker->name(),
        'happy_place' => $faker->name(),
        'for_fun' => $faker->name(),
        'password' => $faker->dateTime->format('His'),

        'created_by' => $faker->numberBetween(1,9),
        'modified_by' => $faker->numberBetween(1,9),
        'deleted_by' => $faker->numberBetween(1,9),
    ];
});
