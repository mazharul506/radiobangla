<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 1/2/2017
 * Time: 11:35 AM
 */
$factory->define(App\Program::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->name,
        'short_description' => $faker->sentence,
        'htmlized_description' => $faker->paragraph,
        'content' => $faker->url,
        'picture' => $faker->imageUrl(),
        'status' => $faker->numberBetween(0,1),
        'created_by' => $faker->numberBetween(1,9),
        'modified_by' => $faker->numberBetween(1,9),
        'deleted_by' => $faker->numberBetween(1,9),
    ];
});
