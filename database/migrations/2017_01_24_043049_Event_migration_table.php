<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventMigrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('eventcategories_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('short_description')->nullable();
            $table->longText('htmlized_description')->nullable();
            $table->date('date');
            $table->datetime('start')->nullable();
            $table->datetime('end')->nullable();
            $table->string('venue')->nullable();
            $table->string('contact')->nullable();
            $table->string('picture')->nullable();
            $table->string('content')->nullable();
            $table->enum('type',array('Paid', 'Free'));
            $table->boolean('status');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->Foreign('eventcategories_id')->references('id')->on('event_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');

    }
}
