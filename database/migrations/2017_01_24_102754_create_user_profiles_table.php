<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('password');
            $table->string('picture');
            $table->string('date_of_birth');
            $table->string('gender');
            $table->integer('nid');
            $table->integer('sid');
            $table->string('contact');
            $table->string('email');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_profiles');
    }
}
