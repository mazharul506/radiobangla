<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration
{
    public function up()
    {
        Schema::create('clients', function(Blueprint $table) {
            $table->increments('id');
            $table->string('category_id');
            $table->string('address');
            $table->string('state');
            $table->string('country');
            $table->string('zip_code');
            $table->string('email');
            $table->string('phone_1');
            $table->string('phone_2');
            $table->string('website');
            $table->string('contact_name');
            $table->string('contact_designation');
            $table->string('contact_phone_1');
            $table->string('contact_phone_2');
            $table->string('contact_email');
            $table->string('assigned_person');
            $table->string('attachment');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}

