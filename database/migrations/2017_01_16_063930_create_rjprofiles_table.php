<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRjprofilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rjprofiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',150)->nullable();
            $table->string('picture',255)->nullable();
            $table->string('email')->nullable();
            $table->string('contact')->nullable();
            $table->date('date_of_birth');
            $table->string('favourite_music',255)->nullable();
            $table->string('tv_shows',255)->nullable();
            $table->string('favourite_places_to_shop',255)->nullable();
            $table->string('happy_place',255)->nullable();
            $table->mediumText('for_fun')->nullable();
            $table->string('password')->unique();

            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rjprofiles');
    }
}
